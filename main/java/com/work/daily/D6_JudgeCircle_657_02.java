package com.work.daily;

/**
 * action: 取消toCharArray()改用charAt.
 *          len 提前求得.
 * result: 性能比01提升不少
 * Created by suk on 2020/8/28.
 */
public class D6_JudgeCircle_657_02 {

    public static void main(String[] args) {
        System.out.println(judgeCircle("UDDU"));
    }
    public static boolean judgeCircle(String moves) {
        int l = 0;
        int r = 0;
        int len = moves.length();
        for (int i = 0; i < len; i++) {
            char c = moves.charAt(i);
            switch (c) {
                case 'L' : ++l;break;
                case 'R' : --l;break;
                case 'U' : ++r;break;
                case 'D' : --r;break;
            }
        }
        return (l == 0 && r == 0);
    }
}
