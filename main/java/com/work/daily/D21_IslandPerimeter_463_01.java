package com.work.daily;

/**
 * solution : 笨方法
 * link : https://leetcode-cn.com/problems/island-perimeter/
 * Created by suk on 2020/10/30
 */
public class D21_IslandPerimeter_463_01 {

    public static void main(String[] args) {
        D21_IslandPerimeter_463_01 d21 = new D21_IslandPerimeter_463_01();
        int[][] grid = {{0,1,0,0},{1,1,1,0},{0,1,0,0},{1,1,0,0}};
        System.out.println(d21.islandPerimeter(grid));
    }
    public int islandPerimeter(int[][] grid) {
        int perimeter = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[i].length; j++) {
                if (grid[i][j] == 0)
                    continue;
                perimeter += 4;
                int left = (j > 0 && grid[i][j-1] == 1) ? 1 : 0;
                int right = (j < grid[i].length - 1 && grid[i][j+1] == 1) ? 1 : 0;
                int up = (i > 0 && grid[i-1][j] == 1) ? 1 : 0;
                int down = (i < grid.length - 1 && grid[i + 1][j] == 1) ? 1 : 0;
                perimeter = perimeter - left - right - up - down;
            }
        }
        return perimeter;
    }
}
