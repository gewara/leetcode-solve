package com.work.zhuanti.joffer;

import java.util.Arrays;

/**
 * title : 字符串的排列
 * solution : 回溯 dfs
 * link : https://leetcode-cn.com/problems/zi-fu-chuan-de-pai-lie-lcof/
 * Created by suk on 2020/9/4.
 */

/**
 * for 选择 in 选择列表:
 # 做选择
 将该选择从选择列表移除
 路径.add(选择)
 backtrack(路径, 选择列表)
 # 撤销选择
 路径.remove(选择)
 将该选择再加入选择列表
 */
public class J38_Permutation_01 {
    public static void main(String[] args) {
        String s = "abc";
        J38_Permutation_01 c = new J38_Permutation_01();
        System.out.println(Arrays.toString(c.permutation(s)));
    }

    String[] result = new String[]{};

    public String[] permutation(String s) {

        char[] chars = s.toCharArray();
        String track = "";

        return new String[]{"1", "2"};
    }

    public void dfs(char[] chars, String track) {
//        if ()
    }

}
