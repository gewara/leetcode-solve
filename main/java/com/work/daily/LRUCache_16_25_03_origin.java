package com.work.daily;

import java.util.HashMap;

/**
 * 设计和构建一个“最近最少使用”缓存，该缓存会删除最近最少使用的项目。缓存应该从键映射到值(允许你插入和检索特定键对应的值)，并在初始化时指定最大容量。当缓存被填满时，它应该删除最近最少使用的项目。

 它应该支持以下操作： 获取数据 get 和 写入数据 put 。

 获取数据 get(key) - 如果密钥 (key) 存在于缓存中，则获取密钥的值（总是正数），否则返回 -1。
 写入数据 put(key, value) - 如果密钥不存在，则写入其数据值。当缓存容量达到上限时，它应该在写入新数据之前删除最近最少使用的数据值，从而为新的数据值留出空间。

 *
 * 方法3. 只借助hashmap和自定义双向链表
 * Created by suk on 2020/8/24.
 */
public class LRUCache_16_25_03_origin {

    private HashMap<Integer, Node> map;
    private DoubleList cache;
    private int capacity;

    public LRUCache_16_25_03_origin(int capacity) {

        this.capacity = capacity;
        map = new HashMap<>();
        cache = new DoubleList();
    }

    public int get(int key) {

        if (!map.containsKey(key))
            return -1;

        int val = map.get(key).val;
        // 将key放置队列头
        put(key, val);
        return val;
    }


    public void put(int key, int value) {
        Node node = new Node(key, value);
        if (map.containsKey(key)) {
            cache.remove(map.get(key));
            cache.addFirst(node);
            map.put(key, node);
        } else {
            if (capacity == cache.size()) {
                Node last = cache.removeLast();
                map.remove(last.key);
            }
            cache.addFirst(node);
            map.put(key, node);
        }

    }

    public static void main(String[] args) {
        LRUCache_16_25_03_origin l = new LRUCache_16_25_03_origin(2);
        l.put(1, 2);
        l.put(2, 3);
        l.put(3, 4);
        l.get(2);
    }

    private class Node {
        public int key, val;
        public Node next, prev;
        public Node(int k, int v) {
            this.key = k;
            this.val = v;
        }
    }

    private class DoubleList {
        // 在链表头部添加节点 x，时间 O(1)
        public void addFirst(Node node) {

        }
        // 删除链表中的 x 节点（x ⼀定存在）
        // 由于是双链表且给的是⽬标 Node 节点，时间 O(1)
        public void remove(Node node) {

        }

        // 删除链表中最后⼀个节点，并返回该节点，时间 O(1)
        public Node removeLast() {
            return null;
        }

        public int size() {
            return cache.size();
        }
    }
}
