package com.work.jike;

import java.util.ArrayDeque;

/**
 *
 给定一个数组 nums，有一个大小为 k 的滑动窗口从数组的最左侧移动到数组的最右侧。你只可以看到在滑动窗口内的
 k个数字。滑动窗口每次只向右移动一位。
 返回滑动窗口中的最大值。
 进阶：
 你能在线性时间复杂度内解决此题吗？
 * Created by suk on 2020/6/30.
 */
public class MaxSlidingWindow_12_239 {

    ArrayDeque<Integer> deq = new ArrayDeque<>();
    int[] nums;

    public static void main(String[] args) {
        int[] nums = {1,3,-1,-3,5,3,6,7};
        int k = 3;
    }
    public int[] maxSlidingWindow(int[] nums, int k) {

        return null;
    }
}
