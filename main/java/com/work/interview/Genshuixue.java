package com.work.interview;

/**
 * Created by suk on 2020/4/29.
 */
public class Genshuixue {

    public static void main(String[] args) {
//        int[] numbers = {1, 3, 5, 7, 8, 25, 4, 20};
        int[] numbers = {1, 3, 5, 7, 8, 25, 4, 20};

        for (int a : solution(numbers))
            System.out.println(a);
    }

    /**
     * 一个数组中的元素，如果其前面的部分等于后面的部分，那么这个点的位序就是平衡点。
     比如列表numbers = [1, 3, 5, 7, 8, 25, 4, 20]，25前面的元素总和为24，25后面的元素总和也是24，那么25就是这个列表的平衡点。
     要求编写程序，寻找并返回任意一个列表的所有平衡点。
     */
    private static int[] solution(int[] numbers) {

        int ri = 0;
        int len = numbers.length;
        int[] result = new int[len];
        int[] before = new int[len];
        int[] after = new int[len];

        for (int j = len - 1; j > 0; j--) {
            if (j == len - 1) {
                after[j] = numbers[len - 1];
                continue;
            }
            after[j] = after[j+1] + numbers[j];
        }
        for (int i = 0; i < len - 2; i++) {
            if (i == 0) {
                before[0] = numbers[0];
                continue;
            }
            before[i] = before[i-1] + numbers[i];
            if (before[i] == after[i + 2])
                result[ri++] = numbers[i + 1];
        }
        return result;

    }
}

