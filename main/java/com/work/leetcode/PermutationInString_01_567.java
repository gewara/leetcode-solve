package com.work.leetcode;

import java.util.Arrays;

/**
 * Given two strings s1 and s2, write a function to return true if s2 contains the permutation of s1. In other words, one of the first string's permutations is the substring of the second string.
 * 超时
 * Created by suk on 2018/10/22.
 */
public class PermutationInString_01_567 {
    public static void main(String[] args) {
        String s1 = "dc";
        String s2 = "bcjabcd";
        PermutationInString_01_567 p = new PermutationInString_01_567();
        System.out.println(p.checkInclusion(s1, s2));
    }

    public boolean checkInclusion(String s1, String s2) {
        s1 = charSort(s1);
        for (int i = 0; i < s2.length() - s1.length() + 1; i++) {
            if (charSort(s2.substring(i, i + s1.length())).equals(s1)) {
                return true;
            }
        }
        return false;
    }

    public String charSort(String string) {
        char[] chars = string.toCharArray();
        Arrays.sort(chars);
        return String.valueOf(chars);
    }
}
