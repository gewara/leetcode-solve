### 一、包目录结构

com.work.daily：每日一题、刷题等。命名规则：

> 1. D4_LetterCombinations_17_01：D4代表第四天；LetterCombinations代表题目；17代表LeetCode题号；01代表第一种解法。
> 2. T167_TwoSum_01：T167 第167题；TwoSum题目名； 01第一种解法。

com.work.compettion：竞赛题。

> W1_J1_ContainsPattern_5499_01：W1第一周；J1竞赛第一题；ContainsPattern题目；5499 LeetCode题号；01代表第一种解法。

com.work.zhuanti：专题刷

com.work.interview：面试题

com.work.jike：极客时间算法专栏里的练习题。

com.work.leetcode：早期刷的一些题目，目前已暂停写入。