package com.work.daily;

import java.util.ArrayList;
import java.util.List;

/**
 * title : 二叉树的中序遍历
 * solution : 递归
 * link : https://leetcode-cn.com/problems/binary-tree-inorder-traversal/
 * Created by suk on 2020/9/14.
 */
public class D12_InorderTraversal_94_01 {

    public static void main(String[] args) {

        D12_InorderTraversal_94_01 d = new D12_InorderTraversal_94_01();

        /**
         * [1,null,2,3]
         */
        TreeNode node = new TreeNode(1);
        node.left = null;
        node.right = new TreeNode(2);
        node.right.left = new TreeNode(3);

        System.out.println(d.inorderTraversal(node));
    }

    List<Integer> res = new ArrayList<>();

    public List<Integer> inorderTraversal(TreeNode root) {
        if (root == null)
            return res;

        if (root.left != null)
            inorderTraversal(root.left);
        res.add(root.val);
        if (root.right != null) {
            inorderTraversal(root.right);
        }
        return res;
    }

    private static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int x) { val = x; }
    }
}
