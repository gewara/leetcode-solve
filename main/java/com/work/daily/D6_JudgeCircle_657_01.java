package com.work.daily;

/**
 * Created by suk on 2020/8/28.
 */
public class D6_JudgeCircle_657_01 {

    public static void main(String[] args) {
        System.out.println(judgeCircle("UDD"));
    }
    public static boolean judgeCircle(String moves) {
        int l = 0;
        int r = 0;

        for (char c : moves.toCharArray()) {
            switch (c) {
                case 'L' : ++l;break;
                case 'R' : --l;break;
                case 'U' : ++r;break;
                case 'D' : --r;break;
            }
        }
        return (l == 0 && r == 0);
    }
}
