package com.work.daily;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * title : 两数组交集
 * Created by suk on 2020/11/2
 */
public class D22_Intersection_349_01 {
    public static void main(String[] args) {
        D22_Intersection_349_01 d = new D22_Intersection_349_01();
        int[] nums1 = {1, 2, 3};
        int[] nums2 = {3};
        System.out.println(Arrays.toString(d.intersection(nums1, nums2)));
    }

    public int[] intersection(int[] nums1, int[] nums2) {
        int[] result;
        Set<Integer> has = new HashSet<>();
        Set<Integer> tmpSave = new HashSet<>();

        for (int num : nums1) {
            has.add(num);
        }
        for (int num : nums2) {
            if (has.contains(num)) {
                tmpSave.add(num);
            }
        }
        result = new int[tmpSave.size()];
        int i = 0;
        for (int num : tmpSave) {
            result[i++] = num;
        }
        return result;
    }
}
