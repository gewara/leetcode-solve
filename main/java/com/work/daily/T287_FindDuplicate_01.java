package com.work.daily;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * title : 给定一个包含 n + 1 个整数的数组 nums，其数字都在 1 到 n 之间（包括 1 和 n），可知至少存在一个重复的整数。假设只有一个重复的整数，找出这个重复的数。
 * solution : 容器记录
 * link : https://leetcode-cn.com/problems/find-the-duplicate-number/
 * Created by suk on 2020/8/28.
 */
public class T287_FindDuplicate_01 {
    public static void main(String[] args) {
        int[] nums = {5, 1 , 2, 2, 5};
        System.out.println(findDuplicate(nums));
    }

    public static int findDuplicate(int[] nums) {

        int len = nums.length;

        Map<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i < len; i++) {
            if (map.containsKey(nums[i]))
                return nums[i];
            map.put(nums[i], new Integer(1));
        }
        return -1;
    }
}
