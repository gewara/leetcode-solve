package com.work.zhuanti.joffer;

import java.util.Stack;

/**
 * title : 利用两个栈实现队列
 * link : https://leetcode-cn.com/problems/yong-liang-ge-zhan-shi-xian-dui-lie-lcof/
 * Created by suk on 2020/9/1.
 */
public class J09_CQueue_01 {

    Stack<Integer> s1;
    Stack<Integer> s2;

    public J09_CQueue_01() {
        s1 = new Stack<>();
        s2 = new Stack<>();
    }

    /**
     * 队列先进先出,栈是先进后出.
     * push直接压入s1.
     * @param value
     */
    public void appendTail(int value) {
        s1.push(value);
    }

    public int deleteHead() {
        if (peek())
            return s2.pop();
        return -1;
    }

    public boolean peek() {
        /**
         * 精华就在判断s2是否为空.不为空则不作s1和s2的转移.只有把s2给清空了,才重新把s1倒灌入s2
         */
        if (s2.isEmpty()) {
            while (!s1.isEmpty()) {
                s2.push(s1.pop());
            }
        }
        if (s2.isEmpty())
            return false;
        return true;
    }
}
