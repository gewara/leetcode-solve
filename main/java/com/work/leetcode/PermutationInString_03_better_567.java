package com.work.leetcode;

/**
 * Given two strings s1 and s2, write a function to return true if s2 contains the permutation of s1. In other words, one of the first string's permutations is the substring of the second string.
 * 已过
 * Created by suk on 2018/10/22.
 */
public class PermutationInString_03_better_567 {
    public static void main(String[] args) {

        String s1 = "adc";
        String s2 = "dcda";

        PermutationInString_03_better_567 p = new PermutationInString_03_better_567();

        System.out.println(p.checkInclusion(s1, s2));
    }

    public boolean checkInclusion(String s1, String s2) {
        if (s2.length() < s1.length())
            return false;
        int[] intArrayS1 = new int[26];
        for (char c : s1.toCharArray()) {
            intArrayS1[c - 'a']++;
        }

        int[] intArrayS2Sub = new int[26];
        for (char c : s2.substring(0, s1.length()).toCharArray()) {
            intArrayS2Sub[c - 'a']++;
        }

        if (judgeEquals(intArrayS1, intArrayS2Sub))
            return true;
        for (int i = 1; i < s2.length() - s1.length() + 1; i++) {

                intArrayS2Sub[s2.charAt(i - 1) - 'a']--;
                intArrayS2Sub[s2.charAt(i + s1.length() - 1) - 'a']++;
                if (judgeEquals(intArrayS1, intArrayS2Sub))
                    return true;
        }
        return false;
    }

    public boolean judgeEquals(int[] i1, int[] i2) {
        if (i1.length != i2.length)
            return false;
        for (int i = 0; i < i1.length; i++) {
            if (i1[i] != i2[i])
                return false;
        }
        return true;
    }

}
