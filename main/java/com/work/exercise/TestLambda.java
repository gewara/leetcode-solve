package com.work.exercise;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Created by suk on 2020/11/14
 */
public class TestLambda {
    public static void main(String[] args) {
        test();
    }
    public static void test() {
        String[] players = {"a", "d", "c"};
        Arrays.sort(players, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareTo(o2);
            }
        });
        System.out.println(Arrays.toString(players));

        Comparator<String> sort = (String s1, String s2) -> (s1.compareTo(s2));
    }
}
