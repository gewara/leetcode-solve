package com.work.leetcode.test;

import java.io.*;

/**
 * Created by suk on 2018/11/16.
 */
public class CountFrequency {
    public static void main(String[] args) throws IOException {
        String str = "";
        String path = "/Users/suk/Documents/sublime/data/data/11-22-monitor121.txt";
        BufferedReader br = new BufferedReader(new FileReader(new File(path)));
        str = br.readLine();
        test(str);
    }

    private static void test(String str) {
        int countLess300 = 0;
        int countMore500 = 0;
        int countMore1000 = 0;
        int countMore2000 = 0;
        int countMore3000 = 0;
        int other = 0;

        for (String s : str.split(",")) {
            int num = Integer.parseInt(s.trim());
            if (num >= 3000) {
                System.out.println(num);
                countMore3000++;
            } else if (num > 2000) {
                countMore2000++;
            } else if (num > 1000) {
                countMore1000++;
            } else if (num > 500) {
                countMore500++;
            } else if (num < 300) {
                countLess300++;
            } else {
                other++;
            }

        }
        System.out.println("countAll=" + str.split(",").length + ",countLess300=" + countLess300 +
        ",countMore500=" + countMore500 + ",countMore1000=" + countMore1000 + ",countMore2000=" + countMore2000
        + ",countMore3000=" + countMore3000 + ",other=" + other);
    }

}
