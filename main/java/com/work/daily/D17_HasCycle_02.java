package com.work.daily;

import java.util.HashSet;
import java.util.Set;

/**
 * 记录走过的节点
 * Created by suk on 2020/10/9
 */
public class D17_HasCycle_02 {
    public boolean hasCycle(ListNode head) {
        // 不要忘记判断
        // if (head == null || head.next == null)
        //     return false;
        Set<ListNode> set = new HashSet<>();
        while (head != null) {
            if (!set.add(head))
                return true;
            head = head.next;
        }
        return false;
    }

    class ListNode {
        int val;
        ListNode next;
        ListNode(int x) {
            val = x;
            next = null;
        }
    }
}
