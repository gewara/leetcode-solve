package com.work.daily;

import java.util.HashMap;
import java.util.Map;

/**
 * 方法三 使用双链表+HashMap

 * Created by suk on 2020/8/24.
 */
public class LRUCache_16_25_LinkedList_HashMap {

    private int capacity;
    private Map<Integer, ListNode> map;
    private ListNode head;
    private ListNode tail;

    public LRUCache_16_25_LinkedList_HashMap(int capacity) {
        this.capacity = capacity;
        map = new HashMap<>();
        head = new ListNode(-1, -1);
        tail = new ListNode(-1, -1);
        head.next = tail;
        tail.pre = head;
    }

    public int get(int key) {
        if (!map.containsKey(key)) {
            return -1;
        }
        ListNode node = map.get(key);
        node.pre.next = node.next;
        node.next.pre = node.pre;
        return -1;
    }

    private void moveToTail(ListNode node) {
        node.pre = tail.pre;
        tail.pre = node;
        node.pre.next = node;
        node.next = tail;
    }
    private class ListNode {
        int key;
        int val;
        ListNode pre;
        ListNode next;

        public ListNode(int key, int val) {
            this.key = key;
            this.val = val;
            pre = null;
            next = null;
        }

    }


    public static void main(String[] args) {
        LRUCache_16_25_LinkedList_HashMap l = new LRUCache_16_25_LinkedList_HashMap(2);
//        l.put(1, 2);
//        l.put(2, 3);
//        l.put(3, 4);
        l.get(2);
    }
}
