package com.work.daily;

/**
 * 龟兔赛跑法
 */
public class D17_HasCycle_01 {
    public boolean hasCycle(ListNode head) {
        // 不要忘记判断
        if (head == null || head.next == null)
            return false;
        ListNode p = head.next;
        while (head != p) {
            // 都是要注意的点
            if (p == null || p.next == null)
                return false;
            head = head.next;
            p = p.next.next;

        }
        return true;
    }

    class ListNode {
        int val;
        ListNode next;
        ListNode(int x) {
            val = x;
            next = null;
        }
    }
}
