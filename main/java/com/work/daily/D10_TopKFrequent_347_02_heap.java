//package com.work.daily;
//
//import java.util.*;
//
///**
// * title : 前 K 个高频元素
// * content : 输入: nums = [1,1,1,2,2,3], k = 2 ;输出: [1,2]
// * solution : 利用堆.method 02和03的构建方式不同
// * link :
// * Created by suk on 2020/9/7.
// */
//public class D10_TopKFrequent_347_02_Heap {
//
//    public static void main(String[] args) {
//        int[] nums = {1, 2, 1, 3, 2, 2, 3, 3, 3};
//        int k = 2;
//        D10_TopKFrequent_347_02_Heap d10 = new D10_TopKFrequent_347_02_Heap();
//        System.out.println(Arrays.toString(d10.topKFrequent(nums, k)));
//    }
//    public int[] topKFrequent(int[] nums, int k) {
//
//        Map<Integer, Integer> map = new HashMap<>();
//        // 注意getOrDefault的用法
//        for (int num : nums) {
//            int i = map.getOrDefault(num, 0);
//            map.put(num, i + 1);
//        }
//
//        // 对value进行排序
//        PriorityQueue<Integer> queue = new PriorityQueue<>(new Comparator<Integer>() {
//            @Override
//            public int compare(Integer o1, Integer o2) {
//                // 这种构造方式,学习了.小顶堆.
//                return map.get(o1) - map.get(o2);
//            }
//        });
//
//        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
//            if (queue.size() < k)
//                queue.offer(entry.getKey());
//            else {
//                // 注意peek和element的区别.
//                // 注意poll和remove的区别.
//                if (entry.getValue() > map.get(queue.peek())) {
//                    queue.poll();
//                    queue.offer(entry.getKey());
//                }
//            }
//        }
//
//        int[] result = new int[k];
//        int i = 0;
//        while (!queue.isEmpty()) {
//            result[i++] = queue.poll();
//        }
//
//        return result;
//    }
//}
