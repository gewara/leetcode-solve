package com.work.jike;

import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * 视频11题,对应LeetCode703题.为了使用大顶堆,将K大改为K小
 *
 * 设计一个找到数据流中第K小元素的类（class）。注意是排序后的第K小元素，不是第K个不同的元素。
 * 你的 KthLargest 类需要一个同时接收整数 k 和整数数组nums 的构造器，它包含数据流中的初始元素。每次调用 KthLargest
 * .add，返回当前数据流中第K大的元素。

 * Created by suk on 2020/6/30.
 */
public class KthLeast_11_703 {

    final PriorityQueue<Integer> q;
    final int k;

    public KthLeast_11_703(int k, int[] a) {
        this.k = k;
        /**
         * 构建大顶堆
         */
        q = new PriorityQueue<>(k, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {

                return o2-o1;
            }
        });
        for (int n : a) {
            add(n);
        }
    }

    public int add(int n) {
        if (q.size() < k) {
            q.offer(n);
        } else if (q.peek() < n) {
            q.poll();
            q.offer(n);
        }
        return q.peek();
    }
}
