package com.work.daily;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class D14_NumJewelsInStones_771_01 {

    public static void main(String[] args) {
        D14_NumJewelsInStones_771_01 d = new D14_NumJewelsInStones_771_01();
        String J = "aB";
        String S = "aaaBBBC";
        // expect=6
        System.out.println(d.numJewelsInStones(J, S));
    }
    public int numJewelsInStones(String J, String S) {
        Set<Character> set = new HashSet<>();
        int count = 0;
        int len = J.length();
        int lenS = S.length();
        for (int i = 0; i < len; i++) {
            set.add(J.charAt(i));
        }
        for (int j = 0; j < lenS; j++) {
            if (set.contains(S.charAt(j)))
                count++;
        }
        return count;

    }
}
