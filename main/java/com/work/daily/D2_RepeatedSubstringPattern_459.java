package com.work.daily;

import java.util.LinkedList;

/**
 * 每日一题第二天:459题
 * 给定一个非空的字符串，判断它是否可以由它的一个子串重复多次构成。给定的字符串只含有小写英文字母，并且长度不超过10000。
 * 示例 1:
 * 输入: "abab"
 * 输出: True
 * 解释: 可由子字符串 "ab" 重复两次构成。
 *
 * Created by suk on 2020/8/24.
 */
public class D2_RepeatedSubstringPattern_459 {

    public static void main(String[] args) {
        String s = "abab";
        System.out.println(simple(s));
    }

    /**
     * 解题思路:
     * 如果s中包含重复的子字符串，那么说明s中至少包含两个子字符串，s+s至少包含4个字串，前后各去掉一位，查找s是否在新构建的字符串中。
     * @param str
     */
    public static boolean simple(String str) {
        String s = str + str;
        return s.substring(1, s.length() - 1).contains(str);
    }
}
