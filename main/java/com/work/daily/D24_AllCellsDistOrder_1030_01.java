package com.work.daily;

import java.util.*;

/**
 * Created by suk on 2020/11/17
 */
public class D24_AllCellsDistOrder_1030_01 {

    public static void main(String[] args) {
        D24_AllCellsDistOrder_1030_01 d = new D24_AllCellsDistOrder_1030_01();
        System.out.println(Arrays.toString(d.allCellsDistOrder(2, 2, 0, 1)));
    }

    //  //lambda here!
    //        listDevs.sort((Developer o1, Developer o2)->o1.getAge()-o2.getAge());
    public int[][] allCellsDistOrder(int R, int C, int r0, int c0) {
        int[][] results = new int[R * C][2];
        Map<int[], Integer> map = new HashMap<>();
        for (int i = 0; i < R; i++) {
            for (int j = 0; j < C; j++) {
                map.put(new int[]{i, j}, calculator(i, j, r0, c0));
            }
        }

        List<Map.Entry<int[], Integer>> list = new ArrayList<>(map.entrySet());
        list.sort((Map.Entry<int[], Integer> o1, Map.Entry<int[], Integer> o2) -> o1.getValue().compareTo(o2.getValue()));
        int i = 0;
        for (Map.Entry entry : list) {
            results[i++] = (int[])entry.getKey();
//            System.out.println(Arrays.toString((int[])entry.getKey()));
        }

        return results;
    }
    public int calculator(int x, int y, int i, int j) {
        return Math.abs(x - i) + Math.abs(y - j);
    }
}
