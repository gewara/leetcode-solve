package com.work.compettion;

/**
 * 8月30号单周竞赛 竞赛题5499
 * Created by suk on 2020/8/30.
 */
public class W1_J1_ContainsPattern_5499_01 {

    public static void main(String[] args) {
        int[] arr = {1, 2, 1, 2};
        int m = 2;
        int k = 2;
        // expect : true
        System.out.println(containsPattern(arr, m, k));
    }

    public static boolean containsPattern(int[] arr, int m, int k) {
        int len = arr.length;

        for (int i = 0; i < len - m; i++) {
            String origin = "";
            String compare = "";
            int count = 1;
            //
            for (int j = i; j <= len - m; j = j + m) {
                for(int l = 0; l < m; l++) {
                    if (j == i)
                        origin = origin + arr[j + l];
                    else
                        compare = compare + arr[j + l];
                }
                if (j == i)
                    ;
                else if (origin.equals(compare)) {
                    count++;
                    compare = "";
                    if (count >= k)
                        return true;
                } else {
                    if (count >= k)
                        return true;
                    else
                        break;
                }
            }


        }
        return false;
    }
}
