package com.work.daily;

import apple.laf.JRSUIUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * links : https://leetcode-cn.com/problems/binary-tree-paths/
 * Created by suk on 2020/9/4.
 */
public class D8_BinaryTreePaths_257_01 {

    public static void main(String[] args) {
        D8_BinaryTreePaths_257_01 d8 = new D8_BinaryTreePaths_257_01();
        /**
         * [1,2,3,null,5]
         *   1
         * 2   3
         *   5
         */
        TreeNode node = new TreeNode(1);
        node.left = new TreeNode(2);
        node.right = new TreeNode(3);
        node.left.left = null;
        node.left.right = new TreeNode(5);

        System.out.println(d8.binaryTreePaths(node));
    }

    List<String> result = new ArrayList<>();


    public List<String> binaryTreePaths(TreeNode root) {
        dfs(root, "");

        return result;
    }

    public void dfs(TreeNode root, String path) {

        if (root != null) {
            StringBuilder track = new StringBuilder(path);
            track.append(root.val);
            if (root.left == null && root.right == null) {

                result.add(track.toString());

            } else {
                track.append("->");
                dfs(root.left, track.toString());

                dfs(root.right, track.toString());
            }

        }

    }

    public static class TreeNode {
        int val;
        TreeNode left, right;
        TreeNode(int x) {
            val = x;
        }
    }
}
