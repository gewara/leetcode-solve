package com.work.daily;

import java.util.LinkedHashMap;
import java.util.Map.Entry;

/**
 * 设计和构建一个“最近最少使用”缓存，该缓存会删除最近最少使用的项目。缓存应该从键映射到值(允许你插入和检索特定键对应的值)，并在初始化时指定最大容量。当缓存被填满时，它应该删除最近最少使用的项目。

 它应该支持以下操作： 获取数据 get 和 写入数据 put 。

 获取数据 get(key) - 如果密钥 (key) 存在于缓存中，则获取密钥的值（总是正数），否则返回 -1。
 写入数据 put(key, value) - 如果密钥不存在，则写入其数据值。当缓存容量达到上限时，它应该在写入新数据之前删除最近最少使用的数据值，从而为新的数据值留出空间。

 *
 * 方法一. 利用LinkedHashMap,重写removeEldestEntry
 * Created by suk on 2020/8/24.
 */
public class LRUCache_16_25 {

    private LinkedHashMap<Integer, Integer> map;
    private int capacity;

    public LRUCache_16_25(int capacity) {

        this.capacity = capacity;
        map = new LinkedHashMap<Integer, Integer>(16, 0.75F, true) {
            @Override
            protected boolean removeEldestEntry(Entry eldest) {
                if (capacity + 1 == map.size()) {
                    return true;
                } else {
                    return false;
                }
            }
        };

    }

    public int get(int key) {
        if (!map.containsKey(key))
            return -1;
        return map.get(key);
    }

    public void put(int key, int value) {
        map.put(key, value);
    }

    public static void main(String[] args) {
        LRUCache_16_25 l = new LRUCache_16_25(2);
        l.put(1, 2);
        l.put(2, 3);
        l.put(3, 4);
        l.get(2);
    }
}
