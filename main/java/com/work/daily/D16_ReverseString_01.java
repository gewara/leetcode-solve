package com.work.daily;

/**
 * title : 反转字符串
 * link : https://leetcode-cn.com/problems/reverse-string/
 */
public class D16_ReverseString_01 {
    public static void main(String[] args) {
        D16_ReverseString_01 d = new D16_ReverseString_01();
        char[] s = {'a', 'b'};
        d.reverseString(s);
        System.out.println(s);
    }
    public void reverseString(char[] s) {
        int len = s.length / 2;
        for (int i = 0; i < len; i++) {
            char tmp;
            tmp = s[s.length - 1 - i];
            s[s.length - 1 - i] = s[i];
            s[i] = tmp;
        }
    }
}
