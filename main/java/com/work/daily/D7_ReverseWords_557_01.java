package com.work.daily;

/**
 * Created by suk on 2020/8/30.
 */
public class D7_ReverseWords_557_01 {

    public static void main(String[] args) {

        String s = "Let's take LeetCode contest";
        // expect :s'teL ekat edoCteeL tsetnoc
        System.out.println(reverseWords(s));
        int a = -Integer.MAX_VALUE;
        int b = 2;
        int c = Integer.MAX_VALUE;
        System.out.println((a + b ) >>> 1);
        System.out.println(a - ((a - b) + c >>> 2));

    }

    public static String reverseWords(String s) {
        StringBuilder sb = new StringBuilder();
        String[] strs = s.split(" ");
        int len;
        for (String str : strs) {
            len = str.length();
            for (int j = len - 1; j >= 0; j--) {
                sb.append(str.charAt(j));
            }
            sb.append(" ");
        }
        return sb.deleteCharAt(sb.length() - 1).toString();
    }
}
