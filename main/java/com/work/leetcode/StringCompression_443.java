package com.work.leetcode;

/**
 * Created by suk on 2018/10/19.
 */
public class StringCompression_443 {
    public int compress(char[] chars) {
        int count = 1;
        int write = 0;
        for (int read = 0; read < chars.length; read++) {

            if (read == chars.length - 1 || chars[read] != chars[read + 1]) {

                chars[write++] = chars[read];
                if (count > 1) {
                    for (char c : String.valueOf(count).toCharArray()) {
                        chars[write++] = c;
                    }
                }
                count = 0;
            }
            count++;
        }
        System.out.println(String.valueOf(chars));
        return write;
    }

    public static void main(String[] args) {
        char[] chars = new char[]{'a', 'b', 'b', 'b', 'c', 'c', 'c', 'd'};
//        char[] chars = new char[]{'c'};
        StringCompression_443 compressChars = new StringCompression_443();
        System.out.println(compressChars.compress(chars));
    }
}
