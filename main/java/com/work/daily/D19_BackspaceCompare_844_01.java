package com.work.daily;

/**
 * Created by suk on 2020/10/19
 */
public class D19_BackspaceCompare_844_01 {
    public static void main(String[] args) {
        D19_BackspaceCompare_844_01 d = new D19_BackspaceCompare_844_01();
        String T = "abc#";
        String S = "abc";

        System.out.println(d.backspaceCompare(T, S));

    }
    public boolean backspaceCompare(String S, String T) {
        StringBuilder sb = new StringBuilder();
        StringBuilder tb = new StringBuilder();
        for (int i = 0; i < S.length(); i++) {
            char tmp = S.charAt(i);
            if (tmp != '#') {
                sb.append(tmp);
            } else {
                if (sb.length() > 0)
                    sb.deleteCharAt(sb.length() - 1);
            }
        }
        for (int j = 0; j < T.length(); j++) {
            char tmp = T.charAt(j);
            if (tmp != '#') {
                tb.append(tmp);
            } else if (tb.length() > 0) {
                tb.deleteCharAt(tb.length() - 1);
            }
        }

        return sb.toString().equals(tb.toString());
    }
}
