package com.work.zhuanti.joffer;

public class J04_FindNumberIn2DArray_01 {
    public static void main(String[] args) {
        J04_FindNumberIn2DArray_01 j = new J04_FindNumberIn2DArray_01();
        //int[][] matrix = {{1,4,7,11,15},{2,5,8,12,19},{3,6,9,16,22},{10,13,14,17,24},{18,21,23,26,30}};
        int[][] matrix = {};
        int target = 20;
        System.out.println(j.findNumberIn2DArray(matrix, target));
    }
    public boolean findNumberIn2DArray(int[][] matrix, int target) {
        int rowLen = matrix.length;
        if (rowLen == 0)
            return false;
        int colLen = matrix[0].length;
        int nowR = 0;
        int nowC = colLen - 1;
        while (nowR < rowLen && nowC >= 0) {
            int num = matrix[nowR][nowC];
            if (target == num)
                return true;
            else if (target > matrix[nowR][nowC]) {
                nowR++;
            } else {
                nowC--;
            }

        }
        return false;

    }
}
