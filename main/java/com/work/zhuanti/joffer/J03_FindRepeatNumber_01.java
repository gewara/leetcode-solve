package com.work.zhuanti.joffer;

/**
 * title : 数组中重复的数字
 * link : https://leetcode-cn.com/problems/shu-zu-zhong-zhong-fu-de-shu-zi-lcof
 * Created by suk on 2020/9/7.
 */
public class J03_FindRepeatNumber_01 {

    public static void main(String[] args) {
        J03_FindRepeatNumber_01 j = new J03_FindRepeatNumber_01();
        int[] nums = {1, 4, 2, 3, 3};
        System.out.println(j.findRepeatNumber(nums));
    }

    /**
     * 参考答案:原地算法
     * @param nums
     * @return
     */
    public int findRepeatNumber(int[] nums) {

        int size = nums.length;
        int temp;
        for (int i = 0; i < size; i++) {
            while (nums[i] != i) {
                if (nums[i] == nums[nums[i]])
                    return nums[i];
                temp = nums[nums[i]];
                nums[nums[i]] = nums[i];
                nums[i] = temp;
            }
        }

        return -1;
    }
}
